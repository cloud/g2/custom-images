# Image purpose


This neutron image deals with following issue:

```console
^C[freznicek@lenovo-t14 ~ 1]$ kubectl -n openstack logs neutron-db-sync-n6bd2
Defaulted container "neutron-db-sync" out of: neutron-db-sync, init (init)
+ neutron-db-manage --config-file /etc/neutron/neutron.conf --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head
INFO  [alembic.runtime.migration] Context impl MySQLImpl.
INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
Traceback (most recent call last):
  File "/var/lib/openstack/lib/python3.8/site-packages/sqlalchemy/engine/base.py", line 1802, in _execute_context
    self.dialect.do_execute(
  File "/var/lib/openstack/lib/python3.8/site-packages/sqlalchemy/engine/default.py", line 732, in do_execute
    cursor.execute(statement, parameters)
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/cursors.py", line 148, in execute
    result = self._query(query)
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/cursors.py", line 310, in _query
    conn.query(q)
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/connections.py", line 548, in query
    self._affected_rows = self._read_query_result(unbuffered=unbuffered)
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/connections.py", line 775, in _read_query_result
    result.read()
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/connections.py", line 1156, in read
    first_packet = self.connection._read_packet()
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/connections.py", line 725, in _read_packet
    packet.raise_for_error()
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/protocol.py", line 221, in raise_for_error
    err.raise_mysql_exception(self._data)
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/err.py", line 143, in raise_mysql_exception
    raise errorclass(errno, errval)
pymysql.err.OperationalError: (1054, "Unknown column 'quotas.project_id' in 'field list'")

The above exception was the direct cause of the following exception:

Traceback (most recent call last):
  File "/var/lib/openstack/bin/neutron-db-manage", line 8, in <module>
    sys.exit(main())
  File "/var/lib/openstack/lib/python3.8/site-packages/neutron/db/migration/cli.py", line 661, in main
    return_val |= bool(CONF.command.func(config, CONF.command.name))
  File "/var/lib/openstack/lib/python3.8/site-packages/neutron/db/migration/cli.py", line 183, in do_upgrade
    run_sanity_checks(config, revision)
  File "/var/lib/openstack/lib/python3.8/site-packages/neutron/db/migration/cli.py", line 645, in run_sanity_checks
    script_dir.run_env()
  File "/var/lib/openstack/lib/python3.8/site-packages/alembic/script/base.py", line 563, in run_env
    util.load_python_file(self.dir, "env.py")
  File "/var/lib/openstack/lib/python3.8/site-packages/alembic/util/pyfiles.py", line 92, in load_python_file
    module = load_module_py(module_id, path)
  File "/var/lib/openstack/lib/python3.8/site-packages/alembic/util/pyfiles.py", line 108, in load_module_py
    spec.loader.exec_module(module)  # type: ignore
  File "<frozen importlib._bootstrap_external>", line 848, in exec_module
  File "<frozen importlib._bootstrap>", line 219, in _call_with_frames_removed
  File "/var/lib/openstack/lib/python3.8/site-packages/neutron/db/migration/alembic_migrations/env.py", line 120, in <module>
    run_migrations_online()
  File "/var/lib/openstack/lib/python3.8/site-packages/neutron/db/migration/alembic_migrations/env.py", line 114, in run_migrations_online
    context.run_migrations()
  File "<string>", line 8, in run_migrations
  File "/var/lib/openstack/lib/python3.8/site-packages/alembic/runtime/environment.py", line 851, in run_migrations
    self.get_context().run_migrations(**kw)
  File "/var/lib/openstack/lib/python3.8/site-packages/alembic/runtime/migration.py", line 608, in run_migrations
    for step in self._migrations_fn(heads, self):
  File "/var/lib/openstack/lib/python3.8/site-packages/neutron/db/migration/cli.py", line 638, in check_sanity
    script.module.check_sanity(context.connection)
  File "/var/lib/openstack/lib/python3.8/site-packages/neutron/db/migration/alembic_migrations/versions/wallaby/expand/f010820fc498_add_unique_quotas_project_resource.py", line 61, in check_sanity
    res = get_duplicate_quotas(connection)
  File "/var/lib/openstack/lib/python3.8/site-packages/neutron/db/migration/alembic_migrations/versions/wallaby/expand/f010820fc498_add_unique_quotas_project_resource.py", line 54, in get_duplicate_quotas
    items = (session.query(quotas.c.project_id, quotas.c.resource)
  File "/var/lib/openstack/lib/python3.8/site-packages/sqlalchemy/orm/query.py", line 2759, in all
    return self._iter().all()
  File "/var/lib/openstack/lib/python3.8/site-packages/sqlalchemy/orm/query.py", line 2894, in _iter
    result = self.session.execute(
  File "/var/lib/openstack/lib/python3.8/site-packages/sqlalchemy/orm/session.py", line 1692, in execute
    result = conn._execute_20(statement, params or {}, execution_options)
  File "/var/lib/openstack/lib/python3.8/site-packages/sqlalchemy/engine/base.py", line 1614, in _execute_20
    return meth(self, args_10style, kwargs_10style, execution_options)
  File "/var/lib/openstack/lib/python3.8/site-packages/sqlalchemy/sql/elements.py", line 325, in _execute_on_connection
    return connection._execute_clauseelement(
  File "/var/lib/openstack/lib/python3.8/site-packages/sqlalchemy/engine/base.py", line 1481, in _execute_clauseelement
    ret = self._execute_context(
  File "/var/lib/openstack/lib/python3.8/site-packages/sqlalchemy/engine/base.py", line 1845, in _execute_context
    self._handle_dbapi_exception(
  File "/var/lib/openstack/lib/python3.8/site-packages/sqlalchemy/engine/base.py", line 2024, in _handle_dbapi_exception
    util.raise_(newraise, with_traceback=exc_info[2], from_=e)
  File "/var/lib/openstack/lib/python3.8/site-packages/sqlalchemy/util/compat.py", line 207, in raise_
    raise exception
  File "/var/lib/openstack/lib/python3.8/site-packages/sqlalchemy/engine/base.py", line 1802, in _execute_context
    self.dialect.do_execute(
  File "/var/lib/openstack/lib/python3.8/site-packages/sqlalchemy/engine/default.py", line 732, in do_execute
    cursor.execute(statement, parameters)
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/cursors.py", line 148, in execute
    result = self._query(query)
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/cursors.py", line 310, in _query
    conn.query(q)
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/connections.py", line 548, in query
    self._affected_rows = self._read_query_result(unbuffered=unbuffered)
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/connections.py", line 775, in _read_query_result
    result.read()
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/connections.py", line 1156, in read
    first_packet = self.connection._read_packet()
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/connections.py", line 725, in _read_packet
    packet.raise_for_error()
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/protocol.py", line 221, in raise_for_error
    err.raise_mysql_exception(self._data)
  File "/var/lib/openstack/lib/python3.8/site-packages/pymysql/err.py", line 143, in raise_mysql_exception
    raise errorclass(errno, errval)
sqlalchemy.exc.OperationalError: (pymysql.err.OperationalError) (1054, "Unknown column 'quotas.project_id' in 'field list'")
[SQL: SELECT quotas.project_id AS quotas_project_id, quotas.resource AS quotas_resource
FROM quotas GROUP BY quotas.project_id, quotas.resource
HAVING count(*) > %(count_1)s]
[parameters: {'count_1': 1}]
(Background on this error at: https://sqlalche.me/e/14/e3q8)
```

