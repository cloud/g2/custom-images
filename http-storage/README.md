# http-storage

The http-storage exposes a storage accessible via HTTP.

## Usage

### Upload

POST + `http://localhost:8000/upload` while payload is sent as 'file' HTTP body parameter.
```
curl -F "file=@/tmp/qwerty" http://localhost:8000/upload/foo/bar
```


### Download
GET  + `http://localhost:8000/download` while payload is sent as 'file' HTTP body parameter.
```
wget  http://localhost:8000/download/foo/bar/qwerty
```



### List of all data
GET  + `http://localhost:8000/list` 
